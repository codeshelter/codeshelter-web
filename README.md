# Code Shelter

[![project chat](https://img.shields.io/badge/zulip-join_chat-brightgreen.svg)](https://codeshelter.zulipchat.com/)
[![pipeline status](https://gitlab.com/codeshelter/codeshelter/badges/master/pipeline.svg)](https://gitlab.com/codeshelter/codeshelter/commits/master)
[![coverage report](https://gitlab.com/codeshelter/codeshelter/badges/master/coverage.svg)](https://gitlab.com/codeshelter/codeshelter/commits/master)



A helpful community of open source maintainers.

## Setting up

To set up the project, you can use docker-compose:

```
pip install docker-compose
docker-compose up
```

After the containers are running, you can add sample data so you can set up some projects by adding the sample fixtures:

```
docker-compose exec web /code/manage.py loaddata sample
```

Afterwards, log in to the admin with the username `admin` and password `admin`.

If you don't want sample data, create a GitHub app and a GitHub OAuth app and add the details to a file called `.env`:

```
GITHUB_ID=youroauthappid
GITHUB_SECRET=youroauthappsecret

GITHUB_APP_ID=yourghappid
GITHUB_WEBHOOK_SECRET=yourghappsecret
```

Log in with GitHub, convert your user to a superuser from the shell, and you're good to go.
